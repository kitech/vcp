module altmain

import vcp.vmm
//import vcp.iohook
import vcp.sched

#flag -Wl,-wrap,main

// wow, it works
fn C.__real_main(int, &byteptr) int

[export: '__wrap_main']
fn __wrap_main(argc int, argv &byteptr) int {
    C.printf("altmain here\n")
    sched.pre_gc_init()
    C.printf("vmm premain init ...\n")
    vmm.pre_main_init()
    // iohook.before_main_init()
    sched.pre_main_init()

    rv := C.__real_main(argc, argv)
    vmm.post_main_deinit()
    return rv
}

