module chan1

fn test_ch1() {
    mut ch := new<i64>(0)
    println(ch)
    ch.sendval(int(5))

    mut val := 123
    ch.recv1(&val)

    ch.recvval(val)

    ch = new<i64>(5)
    println(ch)
}

/*
fn test_ch2() {
    ch := new2<i64>()
    println(ch)
}
*/

