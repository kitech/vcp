
import time
import rand

import vcp.altmain
import vcp.sched
import vcp.mlog
import vcp.futex
import vcp.vmm

fn tcofp(argx voidptr) {
    mlog.info(@FILE, @LINE, argx)
    // more stack
    arr := [1234]voidptr{}
    arr2 := [1234]voidptr{}
    arr3 := [1234]voidptr{}
    for i := 0; i < 300000000000 ;i++ {
        time.sleep_ms(100)
        malloc(int(rand.u32n(34567))+123)
    }
}

fn tcofp2(argx voidptr) {
    mlog.info(@FILE, @LINE, argx)
    // more stack
    arr := [1234]voidptr{}
    arr2 := [1234]voidptr{}
    arr3 := [1234]voidptr{}
    for i := 0; i < 30 ;i++ {
        time.sleep_ms(100)
        malloc(int(rand.u32n(34567))+123)
    }
}

fn main() {
    println("enter user main")
    vmm.start_monitor(100*1024*1024)

    sched.post(tcofp, voidptr(9))
    time.sleep(1)
    sched.post(tcofp, voidptr(8))
    time.sleep(1)
    sched.post(tcofp, voidptr(7))
    time.sleep(1)
    sched.post(tcofp, voidptr(6))
    time.sleep(1)
    sched.post(tcofp, voidptr(5))
    time.sleep(1)

    mlog.info(@FILE, @LINE, "short routines ...")
    for i := 30; ; i++ {
        ensuresleep(5)
        sched.post(tcofp2, voidptr(i))
        // mlog.info(@FILE, @LINE, "sleep EINTR???")
    }
}

fn ensuresleep(n int) {
    btime := time.now()
    for {
        time.sleep(1)
        nowt := time.now()
        if nowt.unix_time() - btime.unix_time() >= n {
            break
        }
    }
}
