package main

import (
	"log"
	"sync"
	"time"
)

func proc_send(stat *Statistic) {
	ch := stat.ch
	// mlog.info(@FILE, @LINE, "send", chx)

	val := 500
	for {
		val += 1
		rv := val
		// ch.send(rv)
		ch <- rv
		stat.sndcnt++
		//time.sleep_ms(1)
	}
}

func proc_recv(stat *Statistic) {
	ch := stat.ch
	// mlog.info(@FILE, @LINE, "recv", chx)

	for {
		rv := 123
		// ch.recv1(&rv)
		rv = <-ch
		_ = rv
		stat.rcvcnt += 1
		// mlog.info(@FILE, @LINE, "got", rv)
	}
}

func donothing(mu sync.Mutex) {
	mu.Lock()
	mu.Unlock()
}
func proc_callempty(stat *Statistic) {
	mu := sync.Mutex{}
	for {
		donothing(mu)
		stat.dntcnt += 1
	}
}

type Statistic struct {
	ch     chan int
	sndcnt int64
	rcvcnt int64
	dntcnt int64
}

func main() {
	stat := &Statistic{}
	ch := make(chan int)
	stat.ch = ch

	// sched.post(proc_recv, stat)
	//time.sleep(1)
	// sched.post(proc_send, stat)

	// go proc_callempty(stat)

	go proc_recv(stat)
	go proc_send(stat)
	go proc_callempty(stat)

	sndcnt := int64(0)
	rcvcnt := int64(0)
	dntcnt := int64(0)
	_, _, _ = sndcnt, rcvcnt, dntcnt
	for i := 0; ; i++ {
		time.Sleep(5 * time.Second)
		// 120w/s???
		// mlog.info(@FILE, @LINE, "stat", stat.sndcnt, stat.rcvcnt, stat.dntcnt,
		//       stat.sndcnt - sndcnt, stat.rcvcnt - rcvcnt, stat.dntcnt-dntcnt)
		log.Println("stat", stat.sndcnt, stat.rcvcnt, stat.dntcnt,
			stat.sndcnt-sndcnt, stat.rcvcnt-rcvcnt, stat.dntcnt-dntcnt)
		sndcnt = stat.sndcnt
		rcvcnt = stat.rcvcnt
		dntcnt = stat.dntcnt

		if i == 10 {
			break
		}
	}
}
