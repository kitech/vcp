module coro

#flag -DCORO_ASM

#include "@VROOT/coro.c"
#include "@VROOT/coroapi.c"

/*
extern void libcoro_create(void* ctx, void* corofp, void* arg, void* sptr, size_t ssze);
extern void libcoro_transfer(void* prev, void* next);
extern void libcoro_destroy (void *ctx);
*/

fn C.libcoro_create(voidptr, voidptr, voidptr, voidptr, size_t) int
fn C.libcoro_transfer(voidptr, voidptr) int
fn C.libcoro_destroy(voidptr) int

pub fn newctx() voidptr {
    cot := malloc(64)
    return cot
}

pub fn create(cot voidptr, fp voidptr, arg voidptr, sptr voidptr, ssze int) {
    C.libcoro_create(cot, fp, arg, sptr, size_t(ssze))
}
pub fn create2(fp voidptr, arg voidptr, sptr voidptr, ssze int) voidptr {
    cot := malloc(64)
    C.libcoro_create(cot, fp, arg, sptr, size_t(ssze))
    return cot
}

fn C.coro_transfer() int
// [inline]
pub fn transfer(prev, next voidptr) {
    // C.libcoro_transfer(prev, next)
    C.coro_transfer(prev, next)
}

pub fn destroy(cot voidptr) {
    C.libcoro_destroy(cot)
}

