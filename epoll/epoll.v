module epoll

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <sys/eventfd.h>

pub union Data {
    pub mut:
    ptr voidptr
    fd int
    u32val u32
    u64val u64
}
pub struct Event {
    pub mut:
    events u32
    data Data
}

pub const (
    IN  = u32(C.EPOLLIN )
    PRI = u32(C.EPOLLPRI)
    OUT = u32(C.EPOLLOUT)
    MSG = u32(C.EPOLLMSG)
    ERR = u32(C.EPOLLERR)
    HUP = u32(C.EPOLLHUP)
    WAKEUP = u32(C.EPOLLWAKEUP)
    ONESHOT = u32(C.EPOLLONESHOT)
    ET = u32(C.EPOLLET)
    EXCLUSIVE = u32(C.EPOLLEXCLUSIVE)
)

pub const (
    CTL_ADD = C.EPOLL_CTL_ADD
    CTL_DEL = C.EPOLL_CTL_DEL
    CTL_MOD = C.EPOLL_CTL_MOD
)

fn C.epoll_create1(int) int
fn C.epoll_ctl(epfd int, op int, fd int, event &Event) int
fn C.epoll_wait(epfd int, events &Event, maxevents int, timeout int) int
fn C.epoll_pwait(epfd int, events &Event, maxevents int, timeout int,
                 sigmask viodptr/*sigset_t*/) int


// int pipe(int pipefd[2]);
// int pipe2(int pipefd[2], int flags);
// fn C.pipe(pipefd &int) int
fn C.pipe2(pipefd &int, flags int) int

// int eventfd(unsigned int initval, int flags);
fn C.eventfd(initval u32, flags int) int


pub fn wait(epfd int, events &Event, maxevents int, timeout int) int {
    return C.epoll_wait(epfd, events, maxevents, timeout)
}

