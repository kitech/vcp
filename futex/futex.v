module futex

import sync
//import sync.atomic2

#flag -D_GNU_SOURCE

#include <stdio.h>
#include <errno.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <linux/futex.h>
#include <stdint.h>
#include <sys/time.h>

fn usemod() {   mu := sync.new_rwmutex() }

[ref_only]
pub struct Futex {
    mut:
    futval u32
}

/*
long futex(uint32_t *uaddr, int futex_op, uint32_t val,
           const struct timespec *timeout,   /* or: uint32_t val2 */
           uint32_t *uaddr2, uint32_t val3);
*/

fn C.syscall() int

const vnil = voidptr(0)

pub fn newFutex() &Futex {
    mut this := &Futex{}
    this.futval = 0 // first wait
    return this
}

[inline]
pub fn futeximpl(uaddr &u32, futex_op int, val u32,
                 timeout voidptr, uaddr2 &u32, val3 u32) int {
    return C.syscall(C.SYS_futex, uaddr, futex_op, val,
                     timeout, uaddr2, val3)
}

[inline]
pub fn futeximpl2(uaddr &u32, futex_op int, val u32) int {
    return C.syscall(C.SYS_futex, uaddr, futex_op, val, vnil, vnil, 0)
}

// vlib/sync/channels.v
// fn C.atomic_compare_exchange_strong_u32(&u32, &u32, u32) bool

pub fn (this &Futex) wait() int {
    for {
        mut spinok := false
        for i in 0..30 {
            one := u32(1)
            ok := C.atomic_compare_exchange_strong_u32(&this.futval, &one, 0)
            if ok {
                spinok = true
                break
            }
            // yield
            C.syscall(C.__NR_sched_yield)//
            // https://chromium.googlesource.com/chromium/src/third_party/WebKit/Source/wtf/+/823d62cdecdbd5f161634177e130e5ac01eb7b48/SpinLock.cpp
            // asm { pause }
        }
        if spinok { break }

        // println("waiting...")
        rv := futeximpl(&this.futval, C.FUTEX_WAIT | C.FUTEX_PRIVATE_FLAG, 0, vnil, vnil, 0)
        eno := C.errno
        if rv == -1 && C.errno != C.EAGAIN {
            panic("futext-FUTEX_WAIT")
        }
        if rv == -1 {
            // println("wtt $eno")
        }
    }
    return 0
}
pub fn (this &Futex) park() int { return this.wait() }

pub fn (this &Futex) wake() int {
    zero := u32(0)

    ok := C.atomic_compare_exchange_strong_u32(&this.futval, &zero, 1)
    if ok {
        rv := futeximpl(&this.futval, C.FUTEX_WAKE | C.FUTEX_PRIVATE_FLAG, 1, vnil, vnil, 0)
        if rv == -1 {
            panic("futex-FUTEX_WAKE")
        }
        return rv
    }
    return 0
}

