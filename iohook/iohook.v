module iohook

import vcp.rtcom

#flag -L@VROOT/
#flag -liohook

#include "@VROOT/libiohook.h"

fn C.iohook_initHook() int

pub fn pre_main_init(y &rtcom.Yielder, allocer voidptr) {
    C.iohook_initHook(y, sizeof(rtcom.Yielder), allocer)
}

pub fn post_main_deinit() {

}

fn init() {
}

// contains real hooked functions
pub struct Functions {
    pub:
    fconnect fn()
    fclose fn()
    fselect fn()
    fpoll fn()
    fepoll fn()
    // sync
    mutex_lock fn(voidptr)
    mutex_unlock fn(voidptr)
    spin_lock fn()
    spin_unlock fn()
}

pub fn funcs() &Functions {
    return voidptr(0)
}

// TODO enum???
pub const (
    YIELD_TYPE_NONE = 0
    YIELD_TYPE_CHAN_SEND =1
    YIELD_TYPE_CHAN_RECV =2
    YIELD_TYPE_CHAN_RECV_CLOSED =3
    YIELD_TYPE_CHAN_SELECT =4
    YIELD_TYPE_CHAN_SELECT_NOCASE =5
    YIELD_TYPE_CONNECT =6
    YIELD_TYPE_READ =7
    YIELD_TYPE_READV =8
    YIELD_TYPE_RECV =9
    YIELD_TYPE_RECVFROM = 10
    YIELD_TYPE_RECVMSG = 11
    YIELD_TYPE_RECVMSG_TIMEOUT = 12
    YIELD_TYPE_WRITE = 13
    YIELD_TYPE_WRITEV = 14
    YIELD_TYPE_SEND = 15
    YIELD_TYPE_SENDTO = 16
    YIELD_TYPE_SENDMSG = 17

    YIELD_TYPE_POLL = 18
    YIELD_TYPE_UUPOLL = 19 // __poll
    YIELD_TYPE_SELECT = 20
    YIELD_TYPE_ACCEPT = 21

    YIELD_TYPE_LOCK = 22
    YIELD_TYPE_TRYLOCK = 23
    YIELD_TYPE_UNLOCK  = 24
    YIELD_TYPE_COND_WAIT = 25
    YIELD_TYPE_COND_TIMEDWAIT = 25

    YIELD_TYPE_SLEEP = 27
    YIELD_TYPE_MSLEEP = 28
    YIELD_TYPE_USLEEP = 29
    YIELD_TYPE_NANOSLEEP = 30

    YIELD_TYPE_GETHOSTBYNAMER = 31
    YIELD_TYPE_GETHOSTBYNAME2R = 32
    YIELD_TYPE_GETHOSTBYADDR = 33
    YIELD_TYPE_GETADDRINFO = 34

    YIELD_TYPE_MAX = 35
)

