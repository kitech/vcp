
#set -e
set -x

CFLAGS="-g -O0 -std=c11 -fPIC -D_GNU_SOURCE -DNRDEBUG -DIOHOOK_XLIB"

#gcc $CFLAGS -g -O0 -fPIC -o hook2.o -c hook2.c
#gcc $CFLAGS -g -O0 -fPIC -o hookcb.o -c hookcb.c
gcc $CFLAGS -g -O0 -fPIC -o hook.o -c hook.c
gcc $CFLAGS -g -O0 -fPIC -o minlog.o -c minlog.c

ar r libiohook.a hook.o minlog.o # hookcb.o


