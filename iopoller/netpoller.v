module iopoller

import sync
import time

import vcp.rtcom
import vcp
import vcp.mlog
import vcp.epoll
import vcp.iohook
import vcp.futex

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <sys/eventfd.h>
#include <signal.h>

const vnil = voidptr(0)

///////////////////

// before vinit
pub fn pre_gc_init() {
}
pub fn pre_main_init(yr &rtcom.Resumer) {
    println("iopoll premain init")
    resumer = yr
}
pub fn pre_main_deinit() {

}
fn init() {
    npl = &NetPoller{}
    npl.init()
}
__global (
    npl = &NetPoller(0)
    resumer = &rtcom.Resumer(0)
)

////////////////
// just Fiber header fields
pub struct FiberMin {
    mut:
    grid int // coroutine id
    mcid int // machine id
}

struct Timeval {
    mut:
    tv_sec i64
    tv_usec i64
}

struct Evdata{
    mut:
    evtype u32
    data &FiberMin = vnil // fiber*
    grid int
    mcid int
    ytype int
    fd i64 // fd or ns or hostname
    out voidptr // void** out; //
    errcode int // int *errcode;
    seqno i64 // long seqno;
    tv Timeval // struct timeval tv; // absolute time
    usec i64 // = tv.tv_sec*USEC + tv.tv_usec
    evt voidptr // struct event* evt;
}
struct Evdata2 {
    mut:
    evtype u32
    dr &Evdata = vnil
    dw &Evdata = vnil
    dt &Evdata = vnil
}

struct NetPoller {
    mut:
    epfd int = -1
    seqno i64 = 10000
    evfds []&Evdata2
    tmupfd [2]int // int tmupfd[2]; // timer add refresh notify epoll_wait pipe
    tmupevfd int = -1
    timers &PQueue = &PQueue{} // []&Evdata2 // in order
    tmerlk &futex.Mutex = futex.newMutex()
    watchers map[string]&Evdata2 // ev_watcher*??? =>
    mu &sync.RwMutex = sync.new_rwmutex()
}


struct PQueue {
    mut:
    items []&Evdata
}
//fn (this &PQueue) len() int { return this.items.len }

pub fn start() {
    go poller_main()
}

// vlib/builtin/cfns.c.v
// fn C.signal() int

fn mysighandler(sig int) int {
    println("signal $sig")
    return 0
}
fn poller_main() {
    // C.signal(C.SIGPWR, mysighandler)
    // C.signal(C.SIGXCPU, mysighandler)
    npl.loop()
}

fn (thisp &NetPoller) init() {
    mut this := thisp

    this.evfds = []&Evdata2{len:999}
    fd := C.epoll_create1(C.EPOLL_CLOEXEC)
    this.epfd = fd

    mut rv := 0
    rv = C.pipe2(this.tmupfd, C.O_NONBLOCK)
    assert rv == 0

    tmupevfd := C.eventfd(0, C.EFD_CLOEXEC|C.EFD_NONBLOCK)
    this.tmupevfd = tmupevfd
    mlog.info(@FILE, @LINE, "epfd $fd $tmupevfd")
}

const (
    SEC = 1
    MSEC = 1000
    USEC = 1000000
    NSEC = 1000000000
)

[typedef]
struct C.sigset_t {}
fn C.sigemptyset() int
fn C.sigaddset() int

fn (thisp &NetPoller) loop() {
    mut this := thisp
    mut rv := 0

    mut evt := epoll.Event{}
    evt.events = epoll.IN
    evt.data.fd = this.tmupevfd

    rv = C.epoll_ctl(this.epfd, epoll.CTL_ADD, this.tmupevfd, &evt)
    assert(rv == 0)

    for {
        revt := epoll.Event{}
        mut timeout := this.next_timeout()
        timeout = if timeout < 0 { MSEC *10} else {timeout}
        timeout = if timeout <= 31 { 31 } else { timeout }

        sigset :=  C.sigset_t{}
        C.sigaddset(&sigset, C.SIGPWR)
        C.sigaddset(&sigset, C.SIGXCPU)
        // mlog.info(@FILE, @LINE, "epoll_waiting ...", timeout)
        rv = epoll.wait(this.epfd, &revt, 1, timeout)
        //rv = C.epoll_pwait(this.epfd, &revt, 1, timeout, &sigset)
        eno := C.errno
        //mlog.info(@FILE, @LINE, "waitret", rv, eno, this.timers.len())
        if rv < 0 {
            if eno == C.EINTR { continue }
            assert(1==2)
        }
        assert(rv >= 0)

        if rv == 0 {
            this.dispatch_timers()
            continue
        }

        evfd := revt.data.fd
        evts := revt.events
        //C.printf("gotevt %d %d %d\n", evfd, evts, this.tmupevfd)
        if evfd == this.tmupevfd {
            val := u64(0)
            for {
                // TODO, maybe hooked, but not coroutine proc???
                rv = C.read(this.tmupevfd, &val, sizeof(u64))
                if rv <= 0 {break}
            }
            this.dispatch_timers()
            continue
        }
        rv = C.epoll_ctl(this.epfd, epoll.CTL_DEL, evfd, vnil)

        // pthread_mutex_lock(&np->evmu);
        d2 := this.evfds[evfd]
        if d2 != vnil {
            this.evfds[evfd] = vnil // clear
            // linfo("clear fd %d\n", evfd);
        }
        // pthread_mutex_unlock(&np->evmu);
        if d2 == vnil {
            // linfo("wtf, fd not found %d\n", evfd);
        }else{
            // extern void netpoller_crnev_globcb(int epfd, int fd, int events, void* cbarg);
            // netpoller_crnev_globcb(np->epfd, evfd, evts, d2);
            // C.printf("evglobcb %d %d %d %p\n", this.epfd, evfd, evts, d2)
            this.evglobcb(this.epfd, evfd, evts, d2)
        }
        this.dispatch_timers()

        if false {
            // linfo("ohno, rv=%d\n", rv);
        }
    }
}

// 1/1000 秒, used in epoll_wait
fn (thisp &NetPoller) next_timeout() int {
    mut timers := thisp.timers
    mut d := &Evdata(vnil)
    mut tmlen := 0
    thisp.tmerlk.mlock()
    tmlen = timers.len()
    if timers.len() > 0 {
        d = timers.items[0]
    }
    thisp.tmerlk.munlock()
    if d != vnil {
        tv := Timeval{}
        C.gettimeofday(&tv, vnil)
        usec := tv.tv_sec * USEC + tv.tv_usec
        diffus := d.usec - usec
        // mlog.info(@FILE, @LINE, "epwait", d.data.grid, d.data.mcid, diffus/1000, tmlen, d.seqno)
        return int(diffus/1000)
    }
    return 500 // -1 // 500
}

// return dispatched count
fn (thisp &NetPoller) dispatch_timers() int {
    mut this := thisp
    if thisp.timers.len() == 0 {
        return 0
    }

    mut tv := Timeval{}
    C.gettimeofday(&tv, vnil)
    usec := tv.tv_sec * USEC + tv.tv_usec

    mut expires := []&Evdata{}
    thisp.tmerlk.mlock()
    for {
        tmcnt := thisp.timers.len()
        if tmcnt == 0 {
            break
        }
        d := thisp.timers.items[0]
        // mlog.info(@FILE, @LINE, d.usec, usec, d.usec <= usec, d.usec-usec)
        if d.usec <= usec {
            // thisp.timers.pop()
            this.timers.items.delete(0)
            expires << d
            // mlog.info(@FILE, @LINE, thisp.timers.len(), expires.len, d.data.mcid)
            //break
        }else {
            break
        }
    }
    thisp.tmerlk.munlock()

    cnt := expires.len
    if cnt == 0 { return cnt }
    len := thisp.timers.len()
    // mlog.info(@FILE, @LINE, "dispatched timers $cnt, left $len")
    for i := 0; i < cnt; i++{
        d := expires[i]
        // mlog.info(@FILE, @LINE, "iopop", d.data.grid, d.data.mcid, len, d.seqno)
        thisp.resume(d)
    }
    for d in expires {} // logic bug?
    return cnt
}

fn (thisp &NetPoller) resume(d &Evdata) {
    assert resumer.resume_one != vnil

    dd := d.data
    ytype := d.ytype
    grid := d.grid
    mcid := d.mcid
    // evdata_free(d);
    // evdata2_free(d2);

    resumer.resume_one(dd, ytype, grid, mcid)
}

fn (thisp &NetPoller) evglobcb(epfd int, fd int, events u32, cbarg &Evdata2) {
    mut np := thisp

    // linfo("fd=%d events=%d cbarg=%p %p\n", fd, events, cbarg, 0);
    mut d2 := cbarg
    assert d2 != vnil
    mut dv := []&Evdata{}

    mut newev := u32(0)
    mut rv := 0
    if d2.evtype == CXEV_TIMER {
        // close(fd)
        dv << d2.dt
        d2.dt = vnil
    } else if d2.evtype == CXEV_IO {
        if (events & epoll.IN) != 0 || (events & epoll.HUP) != 0 || (events & epoll.ERR) != 0 {
            if d2.dr != vnil {
                dv << d2.dr
                d2.dr = vnil
            }
        }
        if (events & epoll.OUT) != 0 || (events & epoll.HUP) != 0 || (events & epoll.ERR) != 0 {
            if d2.dw != vnil {
                dv << d2.dw
                d2.dw = vnil
            }
        }
        if (events & epoll.IN) == 0 && (events & epoll.OUT) == 0  {
            C.printf("woo, close, error??? %d %d %d(%d) %d(%d)\n",
                     fd, events, events&epoll.HUP, epoll.IN, events&epoll.ERR, epoll.OUT)
        }
        if d2.dr != vnil { newev |= epoll.IN }
        if d2.dw != vnil { newev |= epoll.OUT }
        if newev != 0 {
            np.evfds[fd] = d2
        }
        if newev != 0 {
            mut evt := epoll.Event{}
            evt.events = newev | epoll.ET
            evt.data.fd = fd
            rv = C.epoll_ctl(np.epfd, epoll.CTL_ADD, fd, &evt)
        }
    }else{
        C.printf("wtf fd=%d %d %d %d\n", fd, CXEV_IO, CXEV_TIMER, CXEV_DNS_RESOLV)
        // linfo("wtf fd=%d r=%d w=%d\n", fd, events&EPOLLIN, events&EPOLLOUT);
        C.printf("wtf fd=%d %p %d %p %p %p\n", fd, d2, d2.evtype, d2.dr, d2.dw, d2.dt)
        assert(1==2)
    }

    dvcnt := dv.len
    assert(dvcnt > 0)
    for d in dv {
        thisp.resume(d)
    }
}

////// user trigger
const (
    CXEV_IO = u32(0x1 << 5)
    CXEV_TIMER = u32(0x1 << 6)
    CXEV_DNS_RESOLV = u32(0x1 << 7)
)

fn newEvdata(evt u32, gr&FiberMin) &Evdata{
    if gr == vnil { vcp.abort() }
    mut this := &Evdata{}
    this.evtype = evt
    this.data = gr
    this.grid = gr.grid
    this.mcid = gr.mcid
    return this
}
fn newEvdata2(evtype u32) &Evdata2 {
    mut this := &Evdata2{}
    this.evtype = evtype
    return this
}

fn (thisp &PQueue) push(d &Evdata) {
    mut this := thisp
    mut found := false
    for i, item in this.items {
        if d.usec < item.usec {
            this.items.insert(i, d)
            found = true
            break
        }
    }
    if !found { this.items << d }
}
fn (thisp &PQueue) pop() &Evdata {
    mut this := thisp
    if this.items.len == 0 {
        return vnil
    }
    item := this.items[0]
    this.items.delete(0)
    return item
}
fn (thisp &PQueue) first() &Evdata {
    mut this := thisp
    if this.items.len == 0 {
        return vnil
    }
    return this.items[0]
}
fn (thisp &PQueue) len() int { return thisp.items.len }

fn (this &NetPoller) add_timer(ns i64, ytype int, gr &FiberMin) {
    mut np := this

    mut d := newEvdata(CXEV_TIMER, gr)
    d.ytype = ytype
    d.fd = ns
    C.gettimeofday(&d.tv, vnil)
    usec := d.tv.tv_sec*USEC + d.tv.tv_usec + i64(ns/1000)
    d.usec = usec
    d.tv.tv_sec = usec / USEC
    d.tv.tv_usec = usec % USEC
    d.seqno = np.seqno++

    mut rv := 0
    mut rv0 := 0
    // crn_pre_gclock_proc(__func__);
    // pthread_mutex_lock(&np->evmu);
    // rv = pqueue_push(np->timers, d);
    np.tmerlk.mlock()
    rv0 = np.timers.len()
    np.timers.push(d)
    rv = np.timers.len()
    np.tmerlk.munlock()
    // mlog.info(@FILE, @LINE, "iopush", d.data.grid, d.data.mcid, rv0, rv, ns, d.seqno)
    assert rv == rv0+1
    // assert(rv == CC_OK);
    // pthread_mutex_unlock(&np->evmu);
    // crn_post_gclock_proc(__func__);
    mut tmval := u64(1)
    rv = C.write(np.tmupevfd, &tmval, sizeof(u64))
    ok := rv == sizeof(u64)
    if !ok {
        C.printf("add error %d %ld %d\n", rv, ns, gr.grid)
        // evdata2_free(tmer);
        // evdata_free(d);
        // assert(rv == 0);
        return
    }

    // linfo("timer add %d d=%p %ld sec=%d nsec=%d\n", tmfd, d, ns, ts.tv_sec, ts.tv_nsec);
}


fn (this &NetPoller) add_writefd(fd i64, ytype int, gr &FiberMin) {
    mut np := this
    mut d := newEvdata(CXEV_IO, gr)
    d.ytype = ytype
    d.fd = fd

    mut rv := 0
    // crn_pre_gclock_proc(__func__);
    // pthread_mutex_lock(&np->evmu);
    inuse := np.evfds[fd] != vnil
    if inuse {
        mut d2 := np.evfds[fd]
        dwuse := d2.dw != vnil
        samefib := if dwuse { d2.dw.data == gr } else {false}
        mut override := false
        if dwuse && samefib {
        }else{
            override = true
            d2.dw = d
            mut newev := epoll.ET
            if d2.dr != vnil { newev |= epoll.IN }
            newev |= epoll.OUT
            rv = C.epoll_ctl(np.epfd, epoll.CTL_DEL, fd, 0)
            // assert(rv == 0);
            mut evt := epoll.Event{}
            evt.events = newev
            evt.data.fd = int(fd)
            rv = C.epoll_ctl(np.epfd, epoll.CTL_ADD, fd, &evt)
        }
        if dwuse && samefib && !override {
            // ignored operation
        }else{
            C.printf("add w reset %d dwuse=%d samefib=%d override=%d\n",
                   fd, dwuse, samefib, override)
        }
    }else{
        mut d2 := newEvdata2(d.evtype)
        d2.dw = d
        mut evt := epoll.Event{}
        evt.events = epoll.OUT | epoll.ET
        evt.data.fd = int(fd)
        np.evfds[fd] = d2
        rv = C.epoll_ctl(np.epfd, epoll.CTL_ADD, fd, &evt)
        // linfo("add w new %d\n", fd);
    }
    // pthread_mutex_unlock(&np->evmu);
    // crn_post_gclock_proc(__func__);
    if rv != 0 {
        C.printf("add error %d %d %d\n", rv, fd, gr.grid)
        // evdata2_free(evt);
        // evdata_free(d);
        // assert(rv == 0);
        return
    }

    // linfo("evwrite add d=%p %ld\n", d, fd);
}
fn (this &NetPoller) add_readfd(fd i64, ytype int, gr &FiberMin) {
    mut np := this
    mut d := newEvdata(CXEV_IO, gr)
    d.ytype = ytype
    d.fd = fd

    // crn_pre_gclock_proc(__func__);
    mut rv := 0
    // pthread_mutex_lock(&np->evmu);
    inuse := np.evfds[fd] != vnil
    // assert (inuse == 0);
    if inuse {
        mut d2 := np.evfds[fd]
        druse := d2.dr != vnil
        samefib := if druse { d2.dr.data == gr } else {false}
        mut override := false
        if druse && samefib {
            // ignore ok?
        }else{
            override = true
            d2.dr = d
            mut newev := epoll.ET
            if d2.dw != vnil { newev |= epoll.OUT }
            newev |= epoll.IN
            rv = C.epoll_ctl(np.epfd, epoll.CTL_DEL, fd, 0)
            // assert(rv == 0);
            mut evt := epoll.Event{}
            evt.events = newev
            evt.data.fd = int(fd)
            rv = C.epoll_ctl(np.epfd, epoll.CTL_ADD, fd, &evt)
        }
        if druse && samefib && !override {
            // ignored operation
        }else{
            C.printf("add r reset %d druse=%d samefib=%d override=%d\n",
                     fd, druse, samefib, override)
        }
    }else{
        mut d2 := newEvdata2(d.evtype)
        d2.dr = d
        mut evt := epoll.Event{}
        evt.events = epoll.IN | epoll.ET
        evt.data.fd = int(fd)
        np.evfds[fd] = d2
        rv = C.epoll_ctl(np.epfd, epoll.CTL_ADD, fd, &evt)
        // linfo("add r new %d\n", fd);
    }
    // pthread_mutex_unlock(&np->evmu);
    // crn_post_gclock_proc(__func__);
    if rv != 0 {
        C.printf("add error %d %d %d\n", rv, fd, gr.grid)
        // evdata2_free(d2);
        // evdata_free(d);
        // assert(rv == 0);
        return
    }

    if d != vnil {
        // linfo("event_add d=%p fd=%d ytype=%d rv=%d\n", d, fd, ytype, rv);
    }
}

// when ytype is SLEEP/USLEEP/NANOSLEEP, fd is the nanoseconds
pub fn yieldfd(fd i64, ytype int, gr &FiberMin) {
    assert(ytype > iohook.YIELD_TYPE_NONE)
    assert(ytype < iohook.YIELD_TYPE_MAX)

    // struct timeval tv = {0, 123};
    /* match ytype { */
    /*     iohook.YIELD_TYPE_SLEEP {} */
    /*     iohook.YIELD_TYPE_MSLEEP {} */
    /*     iohook.YIELD_TYPE_USLEEP {} */
    /*     iohook.YIELD_TYPE_NANOSLEEP {} */
    /*     // event_base_loopbreak(gnpl__->loop); */
    /*     // event_base_loopexit(gnpl__->loop, &tv); */
    /* } */
    // linfo("fd=%ld, ytype=%d\n", fd, ytype);

    mut ns := i64(0)
    if  ytype == iohook.YIELD_TYPE_SLEEP {
        ns = fd*1000000000
        npl.add_timer(ns, ytype, gr)
    } else if ytype == iohook.YIELD_TYPE_MSLEEP {
        ns = fd*1000000
        npl.add_timer(ns, ytype, gr)
    } else if ytype == iohook.YIELD_TYPE_USLEEP {
        ns = fd*1000
        npl.add_timer(ns, ytype, gr)
    } else if ytype == iohook.YIELD_TYPE_NANOSLEEP{
        ns = fd
        npl.add_timer(ns, ytype, gr)
    } else if ytype == iohook.YIELD_TYPE_CHAN_SEND{
        assert(1==2)// cannot process this type
        npl.add_timer(1000, ytype, gr)
    } else if ytype == iohook.YIELD_TYPE_CHAN_RECV {
        assert(1==2)// cannot process this type
        npl.add_timer(1000, ytype, gr)
    } else if ytype == iohook.YIELD_TYPE_CONNECT ||
        ytype == iohook.YIELD_TYPE_WRITE ||
        ytype == iohook.YIELD_TYPE_WRITEV ||
        ytype == iohook.YIELD_TYPE_SEND ||
        ytype == iohook.YIELD_TYPE_SENDTO ||
        ytype == iohook.YIELD_TYPE_SENDMSG {
            npl.add_writefd(fd, ytype, gr)
            // case YIELD_TYPE_READ: case YIELD_TYPE_READV:
            // case YIELD_TYPE_RECV: case YIELD_TYPE_RECVFROM: case YIELD_TYPE_RECVMSG:
    } else if ytype == iohook.YIELD_TYPE_GETADDRINFO {
        //    netpoller_dnsresolv((char*)fd, ytype, gr);
    } else {
        // linfo("add reader fd=%d ytype=%d=%s\n", fd, ytype, yield_type_name(ytype));
        assert(fd >= 0)
        npl.add_readfd(fd, ytype, gr)
    }
}

