module mlog

#include <@VROOT/mlog/mlog.c>

// see vcp/stdv.
fn C._STR() string

// depend on vcp/builtin/vany.v
// usage: ldebug(@FILE, @LINE, @FN, "msg here")
pub fn info_old_too_many_alloc(file, line string, msgs...Any){
    pfx := logpfxcom(file, line)
    msg := sprinta(msgs...)
    println("${pfx} ${msg}")
    // C.printf("%s %s\n", pfx.str, msg.str)
}

const pathsep = '/'
// usage: logpfxcom(@FILE, @LINE)
fn logpfxcom(file, line string) string {
    mut pos2 := file.last_index(pathsep) or {0}
    pos2 = if pos2 > 0 { pos2 + 1} else {pos2}
    file2 := file[pos2..]
    return '$file2:$line:'
}

// TODO need a non-alloc version
// only msgs have alloc
fn C.mlog_logf2() int
pub fn info(file string, line string, msgs ...Any) string {
    mut sret := string{str: vnil, len: 0, is_lit: 1}
    C.mlog_logf2(&sret, &file, &line, &msgs)
    C.printf("%.*s\n", sret.len, sret.str)
    return sret
}

fn C.mylogf() int
pub fn puts(file string, line string, msgs ...Any) {
    num := msgs.len
    mut sret := string{str: vnil, len: 0, is_lit: 1}
    if num == 0 {
        C.mylogf(&sret, &file, &line, num)
    } else if num == 1 {
        C.mylogf(&sret, &file, &line, num, &msgs[0])
    } else if num == 2 {
        C.mylogf(&sret, &file, &line, num, &msgs[0], &msgs[1])
    } else if num == 3 {
        C.mylogf(&sret, &file, &line, num, &msgs[0], &msgs[1], &msgs[2])
    } else if num == 4 {
        C.mylogf(&sret, &file, &line, num, &msgs[0], &msgs[1], &msgs[2], &msgs[3])
    }
    // println(sret.len)
    //C.printf("loglen %d %s\n", sret.len, sret.str)
}
