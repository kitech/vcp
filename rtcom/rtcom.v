module rtcom

#include "@VROOT/rtcom.c"

pub struct Yielder {
    pub mut:
    incoro fn() int
    getcoro fn() voidptr
    yield fn(i64, int) int
    yield_multi fn(int, int, &i64, &int) int
}

pub struct Resumer {
    pub mut:
    resume_one fn(grobj voidptr, ytype int, grid int, mcid int)
}

pub struct Allocator {
    mallocfn fn(size_t) voidptr
    callocfn fn(size_t, size_t) voidptr
    reallocfn fn(voidptr, size_t) voidptr
    freefn fn(voidptr)
}

fn C.rtcom_pre_gc_init() int

// 被sched调用初始化
pub fn pre_gc_init(yielderx &Yielder, resumerx &Resumer, allocerx &Allocator) {
    C.printf("rtcom pregc init\n")
    C.rtcom_pre_gc_init(yielderx, sizeof(Yielder),
                        resumerx, sizeof(Resumer),
                        allocerx, sizeof(Allocator))
}

// return persistent pointer, can save, not need copy
fn C.rtcom_yielder_get() voidptr
pub fn yielder() &Yielder {
    rv := C.rtcom_yielder_get()
    return &Yielder(rv)
}
fn C.rtcom_resumer_get() voidptr
pub fn resumer() &Resumer {
    rv := C.rtcom_resumer_get()
    return &Resumer(rv)
}
fn C.rtcom_allocator_get() voidptr
pub fn allocer() &Allocator {
    rv := C.rtcom_allocator_get()
    return &Allocator(rv)
}

