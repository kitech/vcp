
### usage

main.v:

```
import time

import vcp.altmain
import vcp.sched

fn routine_func(arg voidptr) {
    for {
        tm := time.now().format_ss()
        println("$arg $tm")
        time.sleep(1)
    }
}

fn main() {
    sched.post(routine_func, voidptr(3))
    for { time.sleep(1) } // not exist main
}
```

### TODO
* [ ] copy stack
* [ ] libgc works hours and stop collect1, only Grow heap to 10264 KiB after 6139888 bytes allocated

