module vcp

fn C.abort() int

pub fn abort() { C.abort() }
