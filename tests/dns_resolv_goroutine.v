
import time
import rand

import vcp.altmain
import vcp.sched
import vcp.mlog
import vcp.futex
import vcp.vmm

import mkuse.vpp.xnet
import mkuse.curlv

fn dns_resolv_goroutine() {
    for {
    xname := xnet.lookup("zhidao.baidu.com") or { panic(err) }
    // mlog.info(@FILE, @LINE, (*xname).str())
    mlog.info(@FILE, @LINE, xname.getip())
    mlog.info(@FILE, @LINE, xname.getips())

    url := "https://zhidao.baidu.com/"
        mut curl := curlv.new().url(url).verbose(true)
        mut ips := xname.getips()
        if ips.len == 0 {
            mlog.info(@FILE, @LINE, "resolv fail", ips)
            time.sleep(5)
            continue
        }
        curl.resolved(ips...)
    mut res := curl.get() or { panic(err) }
        mlog.info(@FILE, @LINE, res.stcode, res.data.len)
    mlog.info(@FILE, @LINE, curl.redirurl())

    res = curl.get() or { panic(err) }
        mlog.info(@FILE, @LINE, res.stcode, res.data.len)
        mlog.info(@FILE, @LINE, "redir", curl.redirurl())

        time.sleep(5)
    }
}
fn main () {
    //dns_resolv_goroutine()
    sched.post(dns_resolv_goroutine, voidptr(0))
    for {
        time.sleep(3)
    }
}

