
import time
import rand

import vcp.altmain
import vcp.sched
import vcp.mlog
import vcp.futex
import vcp.vmm

import mkuse.xlibv

fn cothproc() {
    xh := xlibv.new()
    mlog.info(@FILE, @LINE, "xh")
    xh.open()
    mlog.info(@FILE, @LINE, "xh")
    tray := xh.newtray()
    mlog.info(@FILE, @LINE, voidptr(tray))
    tray.dock()

    //win := xh.dsp.newWindow(0, 0, 300, 200)
    //win.show()
    win := xlibv.newWidget(voidptr(0))
    win.show()

    xh.run(false)
    for {
        time.sleep(3)
    }
    xh.close()
}
fn main() {
    //cothproc()
    sched.post(cothproc, voidptr(0))
    for {
        time.sleep(3)
    }
}
