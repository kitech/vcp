module vcp

import time


// 由于可能被信号中断，需要循环sleep才能保证真的forever
pub fn forsleep() {
    for { time.sleep(100000) }
}
