module vcp

fn test_vany1() {
    printa(123, 3.14, "foo", vnil)
}

struct Object {
    a int
}
fn test_vany2() {
    printa(Object{}.str())
}

fn test_vany3() {
    printa([1,2,3])
}

fn test_vany4() {
    printa(["foo","bar","baz"])
}

fn test_vany5() {
    printa([Any(1), Any("33")])
}
