module vcp

pub fn keep() {}

pub const vnil = voidptr(0)
pub fn nilof<T>() &T { return &T(vnil) }
pub fn nilof2<T>(val T) &T { return &T(vnil) }

/// ctypeconv.v
pub struct CStringArray {
    pub:
    ptr &byteptr = vnil
    len int
    mut:
    vec []byteptr
}

pub fn vsarr2csarr(args []string) &CStringArray {
    mut ca_args := []byteptr{}
    for arg in args {
        p := memdup(arg.str, arg.len)
        ca_args << p
    }
    cp_args := unsafe { &ca_args[0] }
    ao := &CStringArray{cp_args, args.len, ca_args}
    return ao
}

pub fn csarr_nully(args &byteptr) &CStringArray {
    mut len := 0
    for ; ; len ++ {
        p := unsafe { args[len] }
        if p == byteptr(0) {
            break
        }
    }
    return csarr_lengthy(args, len)
}

pub fn csarr_lengthy(args &byteptr, len int) &CStringArray {
    mut vec := []byteptr{}
    ao := &CStringArray{args, len, vec}
    return ao
}

pub fn (csarr &CStringArray) free() {
    for i in 0..csarr.len {
        free(unsafe { csarr.ptr[i] } )
    }
}

pub fn (csarr &CStringArray) at(idx int) byteptr {
    return unsafe { byteptr(csarr.ptr[idx]) }
}

