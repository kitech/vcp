module vmm

pub struct Allocator {
    pub:
    mallocfn fn(int) voidptr
    callocfn fn(int, int) voidptr
    reallocfn fn(voidptr, int) voidptr
    freefn fn(voidptr)
}

pub const allocer_real = Allocator { real_malloc, real_calloc, real_realloc, real_free }
pub const alllocer_gc = Allocator { mallocgc, callocgc, reallocgc, freegc }

