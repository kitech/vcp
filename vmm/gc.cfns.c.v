module vmm

#include <stdio.h>
#include <stdlib.h>
#include "gc/gc.h"

fn C.GC_init()
fn C.GC_deinit()
fn C.GC_malloc(int) voidptr
fn C.GC_realloc(voidptr, int) voidptr
fn C.GC_free(voidptr)
fn C.GC_malloc_uncollectable(int) voidptr
fn C.GC_disable()
fn C.GC_enable()
fn C.GC_MALLOC() voidptr
fn C.GC_REALLOC() voidptr
fn C.GC_FREE()
fn C.GC_MALLOC_UNCOLLECTABLE() voidptr

fn C.GC_is_init_called() int
fn C.GC_is_heap_ptr(voidptr) int
fn C.GC_size(voidptr) int
fn C.GC_allow_register_threads()
fn C.GC_register_finalizer()
fn C.GC_collect_a_little() int
fn C.GC_gcollect()
fn C.GC_is_disabled() int

fn C.GC_set_finalize_on_demand(int)
fn C.GC_set_free_space_divisor(int) // default 3
fn C.GC_set_dont_precollect(int)
fn C.GC_set_dont_expand(int)
fn C.GC_thread_is_registered() int
fn C.GC_register_my_thread() int
fn C.GC_pthread_create() int

/////////////////////
fn C.GC_get_my_stackbottom() voidptr
fn C.GC_set_stackbottom() int
fn C.GC_alloc_lock() int
fn C.GC_alloc_unlock() int

////////////////////
fn C.GC_CreateThread() int

////////////////////
type GC_word = u64

fn C.GC_get_gc_no() size_t
fn C.GC_get_heap_size() size_t
fn C.GC_get_free_bytes() size_t
fn C.GC_get_unmapped_bytes() size_t
fn C.GC_get_bytes_since_gc() size_t
fn C.GC_get_expl_freed_bytes_since_gc() size_t
fn C.GC_get_total_bytes() size_t
fn C.GC_get_heap_usage_safe(pheap_size &GC_word,
                            pfree_bytes &GC_word,
                            punmapped_bytes &GC_word,
                            pbytes_since_gc &GC_word,
                            ptotal_bytes &GC_word)

struct C.GC_prof_stats_s{}
fn C.GC_get_prof_stats() size_t


////////////////////
fn C.GC_version() int
