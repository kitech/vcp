#ifndef LDWRAPMM_H
#define LDWRAPMM_H

extern void* __real_malloc(size_t);
extern void* __real_realloc(void*, size_t);
extern void* __real_calloc(size_t, size_t);
extern void __real_free(void*);

extern int __real_main(int, char**);

#endif /* LDWRAPMM_H */
