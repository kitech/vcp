
module vx

import v.table

fn test_dump_builtin_types () {
    mut vars := varsro
    mut vtab := vars.vtab
    println("=============")
    println(vars.vtab)
    {
        typ := vtab.find_type_idx('int')
        println('int -> $typ')
    }
    {
        typ := vtab.find_type_idx('string')
        println('string -> $typ')
    }
    println("Table.types $vtab.types.len")
    println("=============")
}


fn test_typeof_string2() {
    ty := typeofo("hhh")
    assert ty.len() == sizeof(string)
    assert ty.name() == 'string'
    assert ty.typ() == table.string_type
}
fn test_typeof_voidptr2() {
    ty := typeofo(voidptr(0))
    assert ty.len() == sizeof(voidptr)
    assert ty.name() == 'voidptr'
    assert ty.typ() == table.voidptr_type
}

fn test_typeof_struct2() {
    ty := typeoft<TypeIndex>()
    assert ty.name() == "vx.TypeIndex"
    assert ty.kind2() == table.Kind.struct_
    assert ty.num_fields() == 18
}

fn test_typeof_func() {
    ty := typeof_func(typeof(xyz0)) // not work
    assert ty.num_args() == 3
    assert ty.name() == typeof(xyz0)
    assert ty.kind2() == table.Kind.function
    assert ty.has_return() == true
}

interface Itf123 {
    ok() voidptr
    ok1() i64
    ok2(string)
}
fn test_typeof_interface() {
    ty := typeof_itf<Itf123>()
    assert ty.kind2() == table.Kind.interface_
    assert ty.num_methods() == 3
    assert ty.name() == "vx.Itf123"
    assert ty.len() == 3 * int(sizeof(voidptr))
}

struct FooOk {
    mut: x int
}
fn (fo FooOk) ok() voidptr { return voidptr(0) }
fn (fo FooOk) ok1() i64 { return 0 }
fn (fo FooOk) ok2(s string)  { }
fn xyz0(a int, b i8, c string) f32 { return 0}

fn fwd_test_typeof_interface2(x Itf123) {
    println("==================")
    ty := typeofo(x)
    println("==================")
    println(ty)
}
fn test_typeof_interface2() {
    fo := FooOk{}
    // fwd_test_typeof_interface2(fo)
}

fn test_typeof_method() {
    fo := FooOk{}
    // f := FooOk.ok1 // not work
    // println(fo.ok1) // not work
    // println(typeof(fo.ok1)) // not work
}



